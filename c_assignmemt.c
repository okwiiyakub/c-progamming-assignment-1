#include <stdio.h>
#include <math.h>

void calculateAndDisplayRoots(float a, float b, float c) {
    float discriminant = b * b - 4 * a * c;

    if (discriminant > 0) {
        float root1 = (-b + sqrt(discriminant)) / (2 * a);
        float root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("\n\tRoot 1 = %.2f and Root 2 = %.2f", root1, root2);
    } else if (discriminant == 0) {
        float root1 = -b / (2 * a);
        printf("\n\tRoot 1 = Root 2 = %.2f", root1);
    } else {
        float realPart = -b / (2 * a);
        float imaginaryPart = sqrt(-discriminant) / (2 * a);
        printf("\n\tRoot 1 = %.2f + %.2fi and Root 2 = %.2f - %.2fi", realPart, imaginaryPart, realPart, imaginaryPart);
    }
}

int main() {
    float coefficientA, coefficientB, coefficientC;

    printf("\nFinding the Roots of a Quadratic Equation...!");
    printf("\n\tEnter Coefficients a, b, and c NB:Do Not Enter 0 as the coefficient of a)\n");

    // Read coefficients and check if coefficientA is non-zero
    do {
        scanf("%f", &coefficientA);
        if (coefficientA == 0) {
            printf("\n\tError: Coefficient 'a' must be a Non-Zero value. Please enter a Valid figure.\n");
        }
    } while (coefficientA == 0);

    scanf("%f %f", &coefficientB, &coefficientC);

    calculateAndDisplayRoots(coefficientA, coefficientB, coefficientC);

    return 0;
}